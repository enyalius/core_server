<?php

/**
 * 
 * 
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 */
class ProgramacaoErro extends AplicacaoErro{
    #TODO implementar log para salvar o usuario e a data do programador que conseguir diparar essa exceção =]
    public function ProgramacaoErro($mensagem, $codigoErro = 10) {
        parent::__construct($mensagem, $codigoErro);
        $this->message = $mensagem;
        $this->code = $codigoErro;
    }

}