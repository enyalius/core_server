<?php
namespace core\libs\templateEngine;

/**
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 */
class TemplateEngine extends \Smarty
{

    public function __construct($cache = false) {
        parent::__construct();
        $this->muteExpectedErrors();
        $this->setTemplateDir(TEMPLATES);
        $this->setCompileDir(CACHE . 'templates_c/');
        $this->setConfigDir(CACHE . 'configs/');
        $this->setCacheDir(CACHE. 'cache/');
        $this->caching = $cache;
    }

    public function deploy() {
        echo '<p>' . $this->template_dir . '</p>';
        echo '<p>' . $this->compile_dir . '</p>';
        echo '<p>' . $this->config_dir . '</p>';
        echo '<p>' . $this->cache_dir . '</p>';
        echo '<p>' . $this->caching . '</p>';
    }

    public function ativaDebug(){
        $this->debugging = true;
    }
}
