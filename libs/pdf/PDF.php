<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PDF
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package 
 */
class PDF
{
    public function __construct()
    {        
        define('_MPDF_TTFONTPATH', CACHE . '/ttfonts/'); 		
        define('_MPDF_TTFONTDATAPATH', CACHE . '/templates_c/');
    }}
