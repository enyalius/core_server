<?php
namespace core\libs\login;
#TODO implementar classe.
abstract class Login{

    private $login;
    private $senha;
    private $chave;
    
    private $nivel;
    private $usuario;
    
    /**
     * Construtor que determina qual vai ser a chave criptográfica
     * e qual vai ser a tabela para verificar login e senha
     * 
     * @param String $chave - Chave criptográfica para salgar algoritmo
     * @param String $tabelaLogin
     */
    public function __construct($chave) {
        $this->chave = $chave;
        $this->nivel = 0;
    }

    public abstract function verificaLoginSenha($login, $senha);


    public function getNivelUsuario() {
        return $this->nivel;
    }

    /**
     * Método que verifica se existe um usuário logado.
     * 
     * 
     * @return boolean
     */
    public function verificaLogado() {
        if (true == (isset($_SESSION['usuario_id']) && isset($_SESSION['senha']))) {            
            return $this->verificaIdSenha($_SESSION['usuario_id'], $_SESSION['senha']);
        } else {
            return false;
        }
    }

    private function criptografaSenha($senha) {
        return sha1(md5($this->chave . sha1($senha) . $this->chave));
    }

    private function verificaSenha($senhaUsuario, $senhaBanco) {   
        if(strcmp($this->criptografaSenha($senhaUsuario), $senhaBanco) === 0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Metodo que será chamado na serialização do objeto
     * Este metodo retorna um array com os nomes dos campos que
     * serão guardados
     *
     * @return array
     */
    public function __sleep() {
        return array('idLogin', 'senha');
    }
    
    public function logout(){
        unset($_SESSION['usuario_id']);
        unset($_SESSION['usario_senha']);
    }

}