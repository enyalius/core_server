<?php
namespace core\libs\login;
#TODO implementar classe.
class LoginBanco extends Login{

    private $login;
    private $senha;
    private $chave;
    private $tabelaLogin;
    private $idLogin;
    /**
     * Modelo de conexão ao banco de dados
     * @var Modelo 
     */
    private $modelo;
    private $nivel;
    private $usuario;
    private $email;
    private $campoIdUsuario;
    private $camposAceitosLogin;

    /**
     * Construtor que determina qual vai ser a chave criptográfica
     * e qual vai ser a tabela para verificar login e senha
     * 
     * @param String $chave - Chave criptográfica para salgar algoritmo
     * @param String $tabelaLogin
     */
    public function __construct($chave, \AbstractModel $model, $tabelaLogin = 'usuario') {
        $this->tabelaLogin = $tabelaLogin;
        $this->chave = $chave;
        $this->modelo = $model;
        $this->nivel = 0;
        $this->campoIdUsuario = 'id_usuario';
    }
    
    public function ativaDebug(){
        $this->modelo->ativaDebug();
    }
    
    /**
     * Método que retorna o valor da variável campoIdUsuario
     *
     * @return String - Valor da variável campoIdUsuario
     */
     public function getCampoIdUsuario(){
         return $this->campoIdUsuario;
     }

    /**
     * Método que seta o valor da variável campoIdUsuario
     *
     * @param String $campoIdUsuario - Valor da variável campoIdUsuario
     */
     public function setCampoIdUsuario($campoIdUsuario){
         $campoIdUsuario = trim($campoIdUsuario);
         $this->campoIdUsuario = $campoIdUsuario;
         return true;
     }
    
    /**
     * Retorna apenas o nome de usuário para saudações.
     * 
     * @return boolean
     */
    public function getNomeUsuario(){
        if($this->verificaLogado()){
            $consulta = $this->modelo->consultaUnica(   $this->tabelaLogin,
                                                        'nome_completo',
                                                        $this->campoIdUsuario . ' = ' . $this->idLogin
                                                    );
            $resultado = $this->modelo->resultadoAssoc($consulta);
            return $resultado['nome_completo'];
        }else{
            return false;
        }
    }

    public function getIdUsuario(){
        return $this->idLogin;
    }
    
    public function getUsuario(){
        return $this->usuario;
        
    }
    
    public function getEmail(){
        return $this->email;        
    }
    
    private function montaCondicao($login){
        $login = strtolower($login);
        $cond =  "email ='" . $login . "'";
        foreach ($this->camposAceitosLogin as $campo){
            $cond .=  ' OR '. $campo ." ='". $login . "'";
        }        
        return $cond;
    }
    

    public function verificaLoginSenha($login, $senha) {

        $consulta = $this->modelo->consultaUnica($this->tabelaLogin,
                        'senha, '. $this->campoIdUsuario . ', id_nivel',
                        $this->montaCondicao($login));
        
        if ($this->modelo->numeroLinhas($consulta) == 1) {
            
            $resultado = $this->modelo->resultadoAssoc($consulta);
            $senhaCriptografada = $resultado['senha'];
            $this->modelo->desconecta();
            if ($this->verificaSenha($senha, $senhaCriptografada)) {
                $_SESSION['usuario_id'] = $this->idLogin = $resultado[$this->campoIdUsuario];
                $_SESSION['senha'] = $senha;
                $_SESSION['id_nivel'] = $this->nivel = $resultado['id_nivel'];
                return true; //Autenticação OK
            }
        } else if ($this->modelo->numeroLinhas($consulta) > 1) {
            #TODO Lançar exeção para caso o banco estiver inconsistente.
        }
        return false;
    }

    public function verificaIdSenha($id, $senha) {
        $consulta = $this->modelo->consultaUnica($this->tabelaLogin,
                        'senha, email, '. $this->campoIdUsuario . ', id_nivel, nome_completo',
                       $this->campoIdUsuario . ' = ' . $id);
        if ($this->modelo->numeroLinhas($consulta) == 1) {
            $resultado = $this->modelo->resultadoAssoc($consulta);
            $senhaBanco = $resultado['senha'];

            if ($this->verificaSenha($senha, $senhaBanco)) {
                $this->idLogin = $resultado[$this->campoIdUsuario];
                $_SESSION['id_nivel'] = $resultado['id_nivel'];
                $this->nivel = $resultado['id_nivel'];
                $this->usuario =  new Usuario();
                $this->usuario->setEmail($resultado['email']);
                $this->usuario->setNomeCompleto($resultado['nome_completo']);
                return true; //Autenticação OK
            }
        } else if ($this->modelo->numeroLinhas($consulta) > 1) {
            #TODO Lançar exeção para caso o banco estiver inconsistente.
        }

        return false;
    }

    public function getNivelUsuario() {
        return $this->nivel;
    }

    /**
     * Método que verifica se existe um usuário logado.
     * 
     * 
     * @return boolean
     */
    public function verificaLogado() {
        if (true == (isset($_SESSION['usuario_id']) && isset($_SESSION['senha']))) {            
            return $this->verificaIdSenha($_SESSION['usuario_id'], $_SESSION['senha']);
        } else {
            return false;
        }
    }

    public function criptografaSenha($senha) {
        return sha1(md5($this->chave . sha1($senha) . $this->chave));
    }

    public function criptografaDado($str) {
        for ($i = 0; $i < 8; $i++) {
            $str = strrev(base64_encode($str));
        }
        return $str;
    }

    public function decriptografaDado($str) {
        for ($i = 0; $i < 8; $i++) {
            $str = base64_decode(strrev($str));
        }
        return $str;
    }

    public function verificaInjection($variavel) {

    }

    public function verificaSenha($senhaUsuario, $senhaBanco) {   
        if(strcmp($this->criptografaSenha($senhaUsuario), $senhaBanco) === 0){
            return true;
        }else{
            return false;
        }
    }

    public function recuperaUsuario($usuario, Modelo $modelo) {
        $consulta = $modelo->consultaUnica($this->tabelaLogin, 'usuario',
                        'id_usuario = ' . $this->idLogin);
    }
    
    

    public function setModelo(Modelo $modelo) {
        $this->modelo = $modelo;
    }
    
    public function gravarLogin(){
        $this->modelo->conecta(BANCO, USUARIO_BANCO, SENHA_BANCO);
           
        $this->modelo->atualizar(   $this->tabelaLogin, 
                                    'sys_ultimo_login = now()',
                                    $this->campoIdUsuario . ' = ' . $this->idLogin );
        $this->modelo->desconecta();
    }

    /**
     * Metodo que será chamado na serialização do objeto
     * Este metodo retorna um array com os nomes dos campos que
     * serão guardados
     *
     * @return array
     */
    public function __sleep() {
        return array('idLogin', 'senha');
    }
    
    public function logout(){
        
    }

}