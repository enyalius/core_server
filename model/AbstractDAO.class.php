<?php

/**
 * Description of AbstractDAO
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package 
 */
abstract class AbstractDAO extends AbstractModel
{

    public function save(DTOInterface $object)
    {
        $id = $object->getID();
        if(empty($id)){
            $this->create($object);
        }else{
            $this->update($object);
        }
    }

    /**
     * Método que insere um objeto em sua respectiva tabela no banco de dados
     *
     * @param DTOInterface Objeto data transfer
     */
    public function create(DTOInterface $object)
    {
        return $this->db->insert($object->getTable(), $object->getArrayDados());
    }

    public function getOne(DTOInterface $object)
    {
        return $this->getByID($object->getID());
    }
    
    /**
     * 
     * @param Misc $id
     */
    public abstract function getByID($id);
    
        /**
     * 
     * @param Misc $condicao
     */
    public abstract function getLista($condicao);
    
   /**
     * @return Array  Objetos 
     */
    public function getAll()
    {
        return $this->getLista(false);
    }

    public function update(DTOInterface $object)
    {
        return $this->db->update($object->getTable(), $object->getArrayDados(), $object->getCondition());
    }

    public function delete(DTOInterface $object)
    {
        return $this->db->delete($object->getTable(), $object->getCondition());
    }

    private function isValid()
    {
        
    }

}
