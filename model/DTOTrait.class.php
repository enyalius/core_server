<?php
namespace core\model;
/**
 * Description of DTOTrait
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package
 */
trait DTOTrait
{
    /** 
     * Método responsável por retornar um array com o formato 
     *     "campo_tabela" => "valor" 
     * 
     * para inserir ou atualizar no banco
     * 
     * @return Array - Array de dados para inserir ou atualizar
     */
    public function getArrayDados()
    {
        $campos = array();
        foreach ($this as $chave => $valor) {
            if ((strcasecmp($chave, 'id' . __CLASS__) != 0) && ($chave != 'isValid') && ($chave != 'table')) {               
                $campos[\StringUtil::toUnderscore($chave)] = $valor;
            }
        }
        
        return $campos;
    }
    
    /**
     * Método responsável por popular o objeto recebendo um array no formato
     * "nomeCampo" => "valor" 
     *
     * @param Array $array - Array de 
     * @return Integer - número de erros encontrados
     */
     public function setArrayDados($array){
        $erros = 0;
        foreach($array as $campo => $valor){
            
            $metodo =  'set' .ucfirst($campo);
            if(!$this->{$metodo}($valor)){
                $erros ++;
                $this->isValid = false;
            }
        }
        return $erros;
     }

}
