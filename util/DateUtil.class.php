<?php

/**
 * Classe com métodos 
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package util
 */
class DateUtil
{

    public static function monthYear($input, $format = 'Y-m-d')
    {
        $date = DateTime::createFromFormat($format, $input);
        return $date->format('m/Y');
    }

}
