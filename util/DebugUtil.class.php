<?php

/**
 * Description of DebugUtil
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package 
 */
class DebugUtil
{

    
    public static function show($misc){
        echo '<pre>';
        if(is_array($misc) || is_object($misc)){
            print_r($misc);
        }else{
            var_dump($misc);
        }
        echo '</pre>';
    }
}
