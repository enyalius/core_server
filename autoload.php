<?php

require_once CORE . 'controller/AutoLoader.class.php';
require_once CORE . 'vendor/autoload.php';

use core\controller;

$loader = new controller\AutoLoader();

$loader->register();

$loader->addNamespace('core', CORE );

$loader->addClass('AbstractController', CORE . 'controller/AbstractController');
$loader->addClass('AbstractView', CORE . 'view/AbstractView');

//Model
$loader->addClass('AbstractModel', CORE . 'model/AbstractModel');
$loader->addClass('AbstractDAO', CORE . 'model/AbstractDAO');

//Util
$loader->addClass('StringUtil', CORE . 'util/StringUtil');
$loader->addClass('DebugUtil', CORE . 'util/DebugUtil');
$loader->addClass('ValidatorUtil', CORE . 'util/ValidatorUtil');
$loader->addClass('DateUtil', CORE . 'util/DateUtil');
$loader->addClass('MailUtil', CORE . 'util/MailUtil');

//add Libs common
$loader->addClass('ArquivoUpload', CORE . 'libs/arquivo/ArquivoUpload');
$loader->addClass('Redimensionador', CORE . 'libs/imagem/Redimensionador');



//interfaces
$loader->addClass('DTOInterface', CORE . 'model/DTOInterface');

//Componente
$loader->addClass('Componente', CORE . 'componentes/Componente');

$GLOBALS['loader'] = $loader;

//Chama o AutoLoader de componente
require_once CORE . 'componentes/component_register.php';
