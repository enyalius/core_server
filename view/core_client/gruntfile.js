GLOBAL.PackJS = require('./compiler/PackJS.js').PackJS;

function libJS(lib){

    var destino = (new PackJS(lib)).rootJS;
    var ret = { src: 'js/' + lib + '/' + lib + '.js' };
    if (arguments[1]){
        ret.dest = destino + 'debug/' + lib + '/' + lib + '.js';
       
    }else {
        ret.dest = destino + 'dist/' + lib + '/' + lib + '.js';
    }
    return ret;
}



var mapa = new PackJS('componentes/mapa');
    mapa.origens = [ 'js/componentes/mapa/mapa_objeto.js',
                'js/componentes/mapa/ItemLegenda.js',
                'js/componentes/mapa/ClusterBuilder.js'
        ]
var enyalius = new PackJS('enyalius');
    enyalius.origens = [ 'js/enyalius/verificaNulo.js'];
    
var tabelaManterDados =   new PackJS('componentes/tabela');
   tabelaManterDados.origens = ['js/componentes/tabela/tabela_manter_dados.js'];
   tabelaManterDados.mainFile  = 'tabela_manter_dados';
   
var tabelaRelatorio =   new PackJS('componentes/tabela');
   tabelaRelatorio.origens = ['js/componentes/tabela/tabela_relatorio.js'];
   tabelaRelatorio.mainFile  = 'tabela_relatorio';
   
var jqgrid = new PackJS('jqgrid');
    jqgrid.origens = ['js/jqgrid/jqgrid.js', 'js/jqgrid/i18n/grid.locale-pt-br.js' ];
    
//Inicio do módulo do grunt
module.exports = function(grunt) {
    grunt.initConfig({
        uglify : {
            options : {
                // mangle : false
            },
            
            target : {
                files: [mapa.getCompile(), enyalius.getCompile(), 
                        tabelaManterDados.getCompile(), tabelaRelatorio.getCompile()]
            },
    
            install : {
                files: [
                    libJS('bootstrap'),
                    libJS('jquery'),
                    libJS('openlayers'),
                    jqgrid.getCompile(),
                    jqgrid.getDebug(),
                ]
            }
        }, // uglify

        concat: {
            enyaliusbase: enyalius.getDebug(),
            mapa_lib: mapa.getDebug(),
            tabela_manter_dados: tabelaManterDados.getDebug(),
            tabela_relatorio: tabelaRelatorio.getDebug()
        }, //concat
        
        copy: {
            main: {
                files: [ {  expand: true, src: ['media/**','css/fonts/**'], dest: '../../../www/'},   
                            libJS('openlayers', true),
                             libJS('bootstrap', true)
                        ]
            }
        }, //copy
        
        sass: {
           dist: {
                options: {                       // Target options
                    style: 'compressed'
                },
                files: [{
                    expand: true,
                    cwd: 'css',
                    src: ['**/*.scss', '**/*.css'],
                    dest: '../../../www/css/',
                    ext: '.css'
             }]
           }
        },// sass

        watch : {
            dist : {
                files : [
                        'js/**/*',
                        'css/**/*'
                ],
                tasks : [ 'concat']
            }
        } // watch
    });

    // Plugins do Grunt
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    
    // Tarefas que serão executadas
    grunt.registerTask('default', [ 'concat', 'uglify:target' ]);
    
    // Tarefa para Watch
    grunt.registerTask('w', [ 'watch' ]);
    // Tarefa para install
    grunt.registerTask('i', [ 'concat', 'uglify', 'copy', 'sass' ]);
    grunt.registerTask('s', [ 'sass' ]);
};
