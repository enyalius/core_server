/**
 * verifica se a variável é null 
 * @param {type} variavel
 * @returns {Boolean}
 */
function isNull(variavel) {
    if (typeof (variavel) !== 'undefined' && variavel != null) {
        return true;
    } else {
        return false;
    }
}