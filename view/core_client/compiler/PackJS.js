//Sources para o concat e o uglify
exports.PackJS = function(local){
    this.rootJS = '../../../www/js/';
    this.destino = local;
    this.origens = new Array();
    this.mainFile = 'main';
    
    this.addOrigem = function(){

    };
    
    this.getDistFile = function(){
        return this.rootJS + 'dist/' + this.destino + '/' + this.mainFile + '.js';
    };
    
    this.getDebugFile = function(){
        return this.rootJS + 'debug/' + this.destino + '/' + this.mainFile + '.js';
    };
    
    this.getCompile = function(){
        return {
            src: this.origens,
            dest: this.getDistFile()
        };
    };
    
    this.getDebug = function(){
        return {
            src: this.origens,
            dest: this.getDebugFile()
        };
    };
    
    //private functions
};
